package fr.uvsq.projetfrise.outils;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import fr.uvsq.projetfrise.modele.Date;
import fr.uvsq.projetfrise.modele.Evenement;
import fr.uvsq.projetfrise.modele.Frise;

public class XMLUtils {

    public static void XMLEcriture(File f) {

        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {

            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.newDocument();

            final Element frise = document.createElement("Frise");

            final Element options = document.createElement("Options");

            final Element titreFrise = document.createElement("Titre");
            titreFrise.appendChild(document.createTextNode("test"));
            options.appendChild(titreFrise);

            final Element dateD�but = document.createElement("Date");
            dateD�but.setAttribute("periode", "D�but");
            dateD�but.appendChild(document.createTextNode("2010"));
            options.appendChild(dateD�but);

            final Element dateFin = document.createElement("Date");
            dateFin.setAttribute("periode", "Fin");
            dateFin.appendChild(document.createTextNode("2017"));
            options.appendChild(dateFin);

            final Element periode = document.createElement("Periode");
            periode.appendChild(document.createTextNode("5"));
            options.appendChild(periode);

            frise.appendChild(options);

            document.appendChild(frise);

            final TransformerFactory transformerFactory = TransformerFactory.newInstance();
            final Transformer transformer = transformerFactory.newTransformer();
            final DOMSource source = new DOMSource(document);
            final StreamResult sortie = new StreamResult(f);

            // final StreamResult result = new StreamResult(System.out);

            // prologue

            transformer.setOutputProperty(OutputKeys.VERSION, "1.0");

            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

            // formatage

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            // sortie

            try {
				transformer.transform(source, sortie);
			} catch (TransformerException e) {
				e.printStackTrace();
			}

        }

        catch (final ParserConfigurationException e) {
            e.printStackTrace();
        }

        catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }

	}
	
	public static void XMLEcriture(File f, Frise friseObject) {
		
		System.out.println("Titre : " + friseObject.getTitre());

		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {

			final DocumentBuilder builder = factory.newDocumentBuilder();
			final Document document = builder.newDocument();

			final Element frise = document.createElement("Frise");

			final Element options = document.createElement("Options");

			final Element titreFrise = document.createElement("Titre");
			titreFrise.appendChild(document.createTextNode(friseObject.getTitre()));
			options.appendChild(titreFrise);

			final Element dateDebut = document.createElement("Date");
			dateDebut.setAttribute("periode", "D�but");
			dateDebut.appendChild(document.createTextNode(Integer.toString(friseObject.getDateDebut())));
			options.appendChild(dateDebut);

			final Element dateFin = document.createElement("Date");
			dateFin.setAttribute("periode", "Fin");
			dateFin.appendChild(document.createTextNode(Integer.toString(friseObject.getDateFin())));
			options.appendChild(dateFin);

			final Element periode = document.createElement("Periode");
			periode.appendChild(document.createTextNode(Integer.toString(friseObject.getPeriode())));
			options.appendChild(periode);

			frise.appendChild(options);
			
			for(Evenement e : friseObject.getListeEvents()) {
				
				if(!e.getTitre().isEmpty()) {
					final Element evenement = document.createElement("Evenement");
					evenement.setAttribute("Date", e.getDate().ecrireDate());
					evenement.setAttribute("Importance", Integer.toString(e.getImportance().ordinal()));

					final Element titreEvenement = document.createElement("Titre");
					titreEvenement.appendChild(document.createTextNode(e.getTitre()));
					evenement.appendChild(titreEvenement);

					final Element imageEvenement = document.createElement("CheminImage");
					imageEvenement.appendChild(document.createTextNode(e.getCheminPhoto()));
					evenement.appendChild(imageEvenement);

					final Element descEvenement = document.createElement("Description");
					descEvenement.appendChild(document.createTextNode(e.getDesc()));
					evenement.appendChild(descEvenement);

					frise.appendChild(evenement);
				}
				
			}
			
			document.appendChild(frise);

			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformerFactory.newTransformer();
			final DOMSource source = new DOMSource(document);
			final StreamResult sortie = new StreamResult(f);

			// final StreamResult result = new StreamResult(System.out);

			// prologue

			transformer.setOutputProperty(OutputKeys.VERSION, "1.0");

			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

			// formatage

			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			// sortie

			transformer.transform(source, sortie);

		}

		catch (final ParserConfigurationException e) {
			e.printStackTrace();
		}

		catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}

		catch (TransformerException e) {
			e.printStackTrace();
		}

	}

    public static void XMLLecture(File fileXML) {

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();

            parser.parse(fileXML, new XMLHandler());

        }
        catch (DOMException e) {
            e.printStackTrace();
        }
        catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        catch (TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        }
        catch (SAXException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
