package fr.uvsq.projetfrise.outils;

/**
 * Data
 * 
 * Classe rassemblant des constantes.
 * 
 * @author paul
 *
 */
public class Data {

    public final static String[] TEXTE_BOUTONS = { "<", ">" };

    public final static String[] TEXTE_MENU_ITEM = { "Creation", "Affichage", "Quitter", "?" };
    
    public final static String[] TEXTE_MENU_EVENT = {"Modifier", "Supprimer"};

    public final static byte IMPORTANCE_MAX = 4;

    public final static String INTITULE_CREER_EVENT = "evt";

    public final static String INTITULE_CREER_FRISE = "frise";

    public final static String[] INTITULES_IMPORTANCE = { "IMPORTANT", "FORT", "MOYEN", "FAIBLE" };

}
