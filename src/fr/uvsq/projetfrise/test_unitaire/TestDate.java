package fr.uvsq.projetfrise.test_unitaire;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.uvsq.projetfrise.modele.Date;
import fr.uvsq.projetfrise.outils.ExceptionDate;

public class TestDate {

    @Test
    public void testToString2() {
        String d = "3 avril 1888";
        try {
            Date da = new Date(3, 4, 1888);
            assertEquals("Test TOSTRING", 0, da.toString2().compareTo(d));
        }
        catch (ExceptionDate e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

    @Test
    public void testEstBissextile() {
        assertFalse("annee impaire", Date.estBissextile(2001));
        assertFalse("annee paire non divisible par 4", Date.estBissextile(2002));
        assertTrue("annee divisible par 4 pas par 100", Date.estBissextile(2040));
        assertFalse("annee divisible par 100 pas par 400", Date.estBissextile(2100));
        assertTrue("annee div par 400", Date.estBissextile(2000));
    }

    @Test
    public void testDernierJourDuMois() {
        assertEquals("mois en 31", 31, Date.dernierJourDuMois(1, 2001));
        // on regarde si 31 == Date.dernierJourDuMois(1, 2001)
        assertEquals("mois en 30", 30, Date.dernierJourDuMois(4, 2001));
        assertEquals("mois en 29", 29, Date.dernierJourDuMois(2, 2004));
        assertEquals("mois en 28", 28, Date.dernierJourDuMois(2, 2001));
    }

    @Test
    public void testCompareTo() {
        try {
            Date d1 = new Date(2,2,2002);
            Date d1bis = new Date(2,2,2002);
            Date d2 = new Date(3,2,2002);
            Date d3 = new Date(2,3,2002);
            Date d4 = new Date(3,1,2002);
            Date d5 = new Date(2,2,2003);
            
            assertEquals(0, d1.compareTo(d1bis));//ok
            assertEquals(-1, d1.compareTo(d2));//ok
            assertEquals(1, d2.compareTo(d1));//ok
            assertEquals(1, d5.compareTo(d1));
            assertEquals(-1, d1.compareTo(d5));
            
            assertEquals(1, d3.compareTo(d4));
            assertEquals(-1, d4.compareTo(d3));
        }
        
        catch(ExceptionDate parExc) {
            parExc.printStackTrace() ;
        }
    }

    @Test
    public void testDateDuLendemain() {
        try {
            Date d1 = new Date(2,3,2004);
            Date d1L = new Date(3,3,2004);
            assertEquals("lendemain quelconque", 0, d1.dateDuLendemain().compareTo(d1L));
            
            Date d2 = new Date(31,1,2004);
            Date d2L = new Date(1,2,2004);
            assertEquals("lendemain fin de mois en 31", 0, d2.dateDuLendemain().compareTo(d2L));
            
            Date d3 = new Date(29,2,2004);
            Date d3L = new Date(1,3,2004);
            assertEquals("lendemain fin fev biss", 0, d3.dateDuLendemain().compareTo(d3L));
            
            Date d4= new Date(28,2,2003);
            Date d4L = new Date(1,3,2003);
            assertEquals("lendemain fin fev non biss", 0, d4.dateDuLendemain().compareTo(d4L));
            
            Date d5 = new Date(31,12,2004);
            Date d5L = new Date(1,1,2005);
            assertEquals("lendemain fin annee", 0, d5.dateDuLendemain().compareTo(d5L));
        }
        catch (ExceptionDate parExc){
            parExc.printStackTrace() ;
        }
    }

    @Test
    public void testDateDeLaVeille() {
        try {
            Date d1 = new Date(2,3,2004);
            Date d1L = new Date(3,3,2004);
            assertEquals("lendemain quelconque", 0, d1L.dateDeLaVeille().compareTo(d1));
            
            Date d2 = new Date(31,1,2004);
            Date d2L = new Date(1,2,2004);
            assertEquals("lendemain fin de mois en 31", 0, d2L.dateDeLaVeille().compareTo(d2));
            
            Date d3 = new Date(29,2,2004);
            Date d3L = new Date(1,3,2004);
            assertEquals("lendemain fin fev biss", 0, d3L.dateDeLaVeille().compareTo(d3));
            
            Date d4= new Date(28,2,2003);
            Date d4L = new Date(1,3,2003);
            assertEquals("lendemain fin fev non biss", 0, d4L.dateDeLaVeille().compareTo(d4));
            
            Date d5 = new Date(31,12,2004);
            Date d5L = new Date(1,1,2005);
            assertEquals("lendemain fin annee", 0, d5L.dateDeLaVeille().compareTo(d5));
        }
        catch (ExceptionDate parExc){
            parExc.printStackTrace() ;
        }
    }

}
