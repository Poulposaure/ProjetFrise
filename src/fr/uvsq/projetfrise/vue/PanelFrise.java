package fr.uvsq.projetfrise.vue;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import fr.uvsq.projetfrise.modele.Evenement;
import fr.uvsq.projetfrise.modele.Frise;
import fr.uvsq.projetfrise.modele.ModeleTable;

public class PanelFrise extends JPanel {

    private static final long serialVersionUID = 1L;
    private ModeleTable modele;
    private JTable tableEvent;
    private JScrollPane scrollPane;

    public PanelFrise(Frise parFrise) {
        modele = new ModeleTable(parFrise);
        tableEvent = new JTable(modele);// tableSemaine.setModel(modele)
        tableEvent.setRowHeight(60);// hauteur des lignes

        // un objet se met a l'ecoute des actions de la souris sur la table
        // affiche dans une boite de dialogue l'evenement d'un jour
        tableEvent.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JTable table = (JTable) evt.getSource();
                ModeleTable model = (ModeleTable) table.getModel();
                Point point = evt.getPoint();
                int ligIndex = table.rowAtPoint(point);
                int colIndex = table.columnAtPoint(point);
                //JOptionPane.showMessageDialog(table, model.getValueAt(ligIndex, colIndex));
                JOptionPane.showMessageDialog(table, new ImageIcon((String)model.getValueAt(ligIndex, colIndex)));
            }
        });

        // association du renderer a la table
        tableEvent.setDefaultRenderer(modele.getColumnClass(0), new CelluleRenderer());
        tableEvent.getTableHeader().setFont(new Font("Calibri", Font.BOLD, 19));// entetes plus grand
        tableEvent.getTableHeader().setReorderingAllowed(false);// impossible de changer les colonnes de place
        tableEvent.getTableHeader().setResizingAllowed(false);// impossible de redimensionner les colonnes

        this.add(tableEvent);

        // scrollpane necessaire pour visualiser l'en-tete
        scrollPane = new JScrollPane(tableEvent, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        int periode = parFrise.getDateFin() - parFrise.getDateDebut();
        int nbDefault = 2500;
        
        int nbfinal = nbDefault + (100*periode);
        
        scrollPane.setPreferredSize(new Dimension(nbfinal, 300));
        scrollPane.setMinimumSize(new Dimension(2500, 300));
        scrollPane.setMaximumSize(new Dimension(3000, 300));
        
        this.add(scrollPane);
        
    }

    public void ajoutEvenement(Evenement evt) {
        modele.ajoutEvenement(evt);
        tableEvent = new JTable(modele);// tableSemaine.setModel(modele)
    }
    
    public void setFrise(Frise parFrise) {
        modele = new ModeleTable(parFrise);
        tableEvent.setModel(this.modele);
        
    }

}
