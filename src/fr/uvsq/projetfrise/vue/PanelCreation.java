package fr.uvsq.projetfrise.vue;

import java.util.Calendar;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JYearChooser;

import fr.uvsq.projetfrise.controleur.Controleur;
import fr.uvsq.projetfrise.modele.Date;
import fr.uvsq.projetfrise.modele.Evenement;
import fr.uvsq.projetfrise.modele.Frise;
import fr.uvsq.projetfrise.modele.Importance;
import fr.uvsq.projetfrise.outils.Data;
import fr.uvsq.projetfrise.outils.ExceptionDate;
import fr.uvsq.projetfrise.outils.ExceptionFrise;
import fr.uvsq.projetfrise.outils.LectureEcriture;

public class PanelCreation extends JPanel implements ActionListener {

    /**
     * @author fcledic & prepain
     * @version 0.1
     */
    private static final long serialVersionUID = 1L;

    private JTextField chTitreE;
    private JDateChooser chDateE;
    private JTextArea chDescE;
    private JRadioButton choixImportanceE[];
    private ButtonGroup importancesE;
    private JComboBox<Integer> eventImportance;
    private int choix;
    //private JTextField chPathPhotoE;
    private String chPathPhotoE;
    private JButton ouvrirFileChooser;
    private JButton creerEvent;

    private JTextField chTitreF;
    private JYearChooser anneeDebut;
    private JYearChooser anneeFin;
    private JComboBox<Integer> periode;
    private JButton creerFrise;

    private File choisirFichier;

    public PanelCreation(File f) {

        JPanel panelEst = new JPanel(); // panel pour la creation d'evenements
        JPanel panelOuest = new JPanel(); // panel pour la creation de frises

        this.setLayout(new GridLayout(1, 2, 0, 0));// 1 ligne, 2 colonnes

        // Creation d'un evenement

        panelEst.setLayout(new GridBagLayout());
        GridBagConstraints contraintesEvent = new GridBagConstraints();
        contraintesEvent.anchor = GridBagConstraints.WEST;
        contraintesEvent.insets = new Insets(9, 9, 9, 9);

        JLabel titrePanelEvent = new JLabel("Creation d'un evenement");
        JLabel titre = new JLabel("Titre");
        JLabel date = new JLabel("Date");
        JLabel description = new JLabel("Description");
        JLabel poids = new JLabel("Poids");
        JLabel chemin = new JLabel("Chemin de la photo");

        chTitreE = new JTextField(10);
        chDateE = new JDateChooser();
        chDescE = new JTextArea(8, 10);
        chDescE.setLineWrap(true);
        chDescE.setWrapStyleWord(true);
        Integer poidsJCombo[] = { 0, 1, 2, 3 };
        choixImportanceE = new JRadioButton[Data.IMPORTANCE_MAX];

        importancesE = new ButtonGroup();

        eventImportance = new JComboBox<Integer>(poidsJCombo);
        eventImportance.setSelectedItem(poidsJCombo[1]);
        //chPathPhotoE = new JTextField(10);
        ouvrirFileChooser = new JButton("Choisir une photo");

        creerEvent = new JButton("Creer un evenement");
        creerEvent.setContentAreaFilled(false);
        creerEvent.setActionCommand(Data.INTITULE_CREER_EVENT);
        creerEvent.setEnabled(false);

        if (f != null) {
            choisirFichier = f;
        }
        else {
            //			System.out.println("Ouvrir 2");
            //			choisirFichier = LectureEcriture.getFile(this);
        }

        panelEst.add(titrePanelEvent, contraintesEvent);
        contraintesEvent.gridy++;

        contraintesEvent.gridy++;
        contraintesEvent.gridx = 0;
        panelEst.add(titre, contraintesEvent);
        contraintesEvent.gridwidth = 4;
        contraintesEvent.gridx++;
        contraintesEvent.fill = GridBagConstraints.HORIZONTAL;
        panelEst.add(chTitreE, contraintesEvent);
        contraintesEvent.fill = GridBagConstraints.NONE;

        contraintesEvent.gridy++;
        contraintesEvent.gridx = 0;
        panelEst.add(date, contraintesEvent);
        contraintesEvent.gridwidth = 4;
        contraintesEvent.gridx++;
        contraintesEvent.fill = GridBagConstraints.HORIZONTAL;
        panelEst.add(chDateE, contraintesEvent);
        contraintesEvent.fill = GridBagConstraints.NONE;

        contraintesEvent.gridy++;
        contraintesEvent.gridx = 0;
        panelEst.add(description, contraintesEvent);
        contraintesEvent.gridwidth = 4;
        contraintesEvent.gridx++;
        contraintesEvent.fill = GridBagConstraints.HORIZONTAL;
        panelEst.add(chDescE, contraintesEvent);
        contraintesEvent.fill = GridBagConstraints.NONE;

        contraintesEvent.gridy++;
        contraintesEvent.gridx = 0;
        panelEst.add(poids, contraintesEvent);
        contraintesEvent.gridwidth = 1;
        contraintesEvent.gridx++;
        for (int i = 0; i < choixImportanceE.length; i++) {
            choixImportanceE[i] = new JRadioButton(Data.INTITULES_IMPORTANCE[i], i == 0);
            choixImportanceE[i].setActionCommand(Integer.toString(i));
            choixImportanceE[i].setContentAreaFilled(false);
            choixImportanceE[i].addActionListener(this);
            importancesE.add(choixImportanceE[i]);
            panelEst.add(choixImportanceE[i], contraintesEvent);
            if (i != 1)
                contraintesEvent.gridx++;
            else {
                contraintesEvent.gridx = 1;
                contraintesEvent.gridy++;
            }

        }

        contraintesEvent.gridy++;
        contraintesEvent.gridx = 0;
        panelEst.add(chemin, contraintesEvent);
        contraintesEvent.gridwidth = 4;
        contraintesEvent.gridx++;
        contraintesEvent.fill = GridBagConstraints.HORIZONTAL;
        panelEst.add(ouvrirFileChooser, contraintesEvent);
        ouvrirFileChooser.addActionListener(this);
        contraintesEvent.fill = GridBagConstraints.NONE;

        contraintesEvent.gridy++;
        contraintesEvent.gridx = 0;
        panelEst.add(creerEvent, contraintesEvent);

        // =-=-=-=-=-PanelCreationFrise-=-=-=-=-=

        panelOuest.setLayout(new GridBagLayout());
        GridBagConstraints contraintesFrise = new GridBagConstraints();
        contraintesFrise.anchor = GridBagConstraints.WEST;
        contraintesFrise.insets = new Insets(13, 13, 13, 13);

        JLabel titrePanelFrise = new JLabel("Creation de la frise", JLabel.LEFT);
        JLabel labelTitreFrise = new JLabel("Titre de la frise", JLabel.LEFT);
        JLabel labelAnneeDebut = new JLabel("Date de debut", JLabel.LEFT);
        anneeDebut = new JYearChooser();
        JLabel labelAnneeFin = new JLabel("Date de fin", JLabel.LEFT);
        anneeFin = new JYearChooser();
        JLabel labelPeriode = new JLabel("Periode", JLabel.LEFT);
        Integer periodeJCombo[] = { 0, 1, 2, 3, 4, 5 };
        periode = new JComboBox<Integer>(periodeJCombo);
        chTitreF = new JTextField(10);
        creerFrise = new JButton("Creer une frise");
        creerFrise.setContentAreaFilled(false);
        creerFrise.setActionCommand(Data.INTITULE_CREER_FRISE);

        panelOuest.add(titrePanelFrise, contraintesFrise);
        contraintesFrise.gridy++;

        contraintesFrise.gridy++;
        contraintesFrise.gridx = 0;
        panelOuest.add(labelTitreFrise, contraintesFrise);
        contraintesFrise.gridwidth = 4;
        contraintesFrise.gridx++;
        contraintesFrise.fill = GridBagConstraints.HORIZONTAL;
        panelOuest.add(chTitreF, contraintesFrise);
        contraintesFrise.fill = GridBagConstraints.NONE;

        contraintesFrise.gridy++;
        contraintesFrise.gridx = 0;
        panelOuest.add(labelAnneeDebut, contraintesFrise);
        contraintesFrise.gridwidth = 1;
        contraintesFrise.gridx++;
        contraintesFrise.fill = GridBagConstraints.HORIZONTAL;
        panelOuest.add(anneeDebut, contraintesFrise);
        contraintesFrise.fill = GridBagConstraints.NONE;

        contraintesFrise.gridy++;
        contraintesFrise.gridx = 0;
        panelOuest.add(labelAnneeFin, contraintesFrise);
        contraintesFrise.gridwidth = 1;
        contraintesFrise.gridx++;
        contraintesFrise.fill = GridBagConstraints.HORIZONTAL;
        panelOuest.add(anneeFin, contraintesFrise);
        contraintesFrise.fill = GridBagConstraints.NONE;

        contraintesFrise.gridy++;
        contraintesFrise.gridx = 0;
        panelOuest.add(labelPeriode, contraintesFrise);
        contraintesFrise.gridwidth = 1;
        contraintesFrise.gridx++;
        contraintesFrise.fill = GridBagConstraints.HORIZONTAL;
        panelOuest.add(periode, contraintesFrise);
        contraintesFrise.fill = GridBagConstraints.NONE;

        contraintesEvent.gridy++;
        contraintesEvent.gridx = 0;
        panelOuest.add(creerFrise, contraintesEvent);

        // =-=-=-=-=-PanelCreation-=-=-=-=-=


        this.add(panelOuest);
        this.add(panelEst);

    }

    public Frise getFrise() throws ExceptionFrise {
        // recupere titre, dateDebut, dateFin, listeEvts(vide)
        String titre = chTitreF.getText();
        int anneeDebut = this.anneeDebut.getYear();
        int anneeFin = this.anneeFin.getYear();
        //chDateE.setSelectableDateRange(new java.util.Date(anneeDebut), new java.util.Date(anneeFin));//marche pas
        return new Frise(titre, anneeDebut, anneeFin, periode.getSelectedIndex());
    }

    public Evenement getEvenement() throws ExceptionDate {
        // recupere titre, date, description, description, importance, chemin de la photo

        String chTitreE = this.chTitreE.getText();
        Calendar cal = chDateE.getCalendar();
        Date dateEvt = new Date(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
        String description = chDescE.getText();
        //Importance importance = Importance.values()[this.eventImportance.getSelectedIndex()];
        Importance importance = Importance.values()[choix];
        System.out.println("CHOIX : " + choix);
        //String chemin = chPathPhotoE.getText();
        //String chemin = choisirFichier.getAbsolutePath();
        return new Evenement(chTitreE, dateEvt, description, importance, chPathPhotoE);
    }

    public void enregistreEcouteur(Controleur parC) {
        creerEvent.addActionListener(parC);
        creerFrise.addActionListener(parC);
    }// enregistreEcouteur()

    public void reset() {
        this.chTitreE.setText(new String());
        this.chDescE.setText(new String());
        this.chTitreF.setText(new String());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        choix = Integer.parseInt(importancesE.getSelection().getActionCommand());
        if (e.getSource() == ouvrirFileChooser) {
            File o = LectureEcriture.getImage(this);
            if (o!=null) {
                chPathPhotoE = o.getAbsolutePath();
                creerEvent.setEnabled(true);
            }
        }

    }

    public JButton getCreerEvent() {
        return creerEvent;
    }

    public void setCreerEvent(JButton creerEvent) {
        this.creerEvent = creerEvent;
    }

    public JButton getCreerFrise() {
        return creerFrise;
    }

    public void setCreerFrise(JButton creerFrise) {
        this.creerFrise = creerFrise;
    }



}
