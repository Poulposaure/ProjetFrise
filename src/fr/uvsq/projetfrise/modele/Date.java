package fr.uvsq.projetfrise.modele;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.StringTokenizer;

import fr.uvsq.projetfrise.outils.ExceptionDate;

/**
 * Date
 * 
 * 
 * 
 * 
 * @author flcledic
 *
 */
public class Date implements Comparable<Date>, Serializable {

    private static final long serialVersionUID = 1L;
    private int Heure;
    private int Minute;

    private int jour;
    private int mois;
    private int annee;
    private int jourSemaine;
    private int semaine;
    HashMap<Integer, String> nomJourSemaine = new HashMap<Integer, String>();
    HashMap<Integer, String> nomMois = new HashMap<Integer, String>();

    //!!! Mois compris entre 1 et 12 !!!

    private void init() {
        // Initialisation des jours de la semaine
        this.nomJourSemaine.put(1, "dimanche");
        this.nomJourSemaine.put(2, "lundi");
        this.nomJourSemaine.put(3, "mardi");
        this.nomJourSemaine.put(4, "mercredi");
        this.nomJourSemaine.put(5, "jeudi");
        this.nomJourSemaine.put(6, "vendredi");
        this.nomJourSemaine.put(7, "samedi");

        // Initialisation des Mois

        this.nomMois.put(1, "janvier");
        this.nomMois.put(2, "f�vrier");
        this.nomMois.put(3, "mars");
        this.nomMois.put(4, "avril");
        this.nomMois.put(5, "mai");
        this.nomMois.put(6, "juin");
        this.nomMois.put(7, "juillet");
        this.nomMois.put(8, "aout");
        this.nomMois.put(9, "septembre");
        this.nomMois.put(10, "octobre");
        this.nomMois.put(11, "novembre");
        this.nomMois.put(12, "d�cembre");
    }

    /**
     * Constructeur de la Date.
     * @param jour
     * @param mois
     * @param annee
     * @param heure
     * @param minute
     * @throws ExceptionDate
     */
    public Date(int jour, int mois, int annee, int heure, int minute) throws ExceptionDate {// Initialise
        // l'objet
        init();

        if (annee < 1583) {
            throw new ExceptionDate("L'ann�e doit �tre sup�rieur � 1583");
        }
        if (mois <= 0 || mois > 12) {
            throw new ExceptionDate("Le Mois doit �tre compris entre 1 et 12");
        }
        if (jour < 0 || jour > dernierJourDuMois(mois, annee)) {
            throw new ExceptionDate("Le jour doit �tre compris entre 1 et " + dernierJourDuMois(mois, annee));
        }
        GregorianCalendar calendrier = new GregorianCalendar(annee, mois - 1, jour, heure, minute);
        setJour(jour);
        setMois(mois);
        setAnnee(annee);
        // System.out.println("tt " + calendrier.get(Calendar.DAY_OF_WEEK));
        // setJourSemaine(calendrier.get(Calendar.DAY_OF_WEEK));

        this.jourSemaine = calendrier.get(Calendar.DAY_OF_WEEK);
        this.semaine = calendrier.get(Calendar.WEEK_OF_YEAR);
        this.Heure = calendrier.get(Calendar.HOUR_OF_DAY);
        this.Minute = calendrier.get(Calendar.MINUTE);
    }

    /**
     * Constructeur de la Date.
     * @param jour
     * @param mois
     * @param annee
     * @throws ExceptionDate
     */
    public Date(int jour, int mois, int annee) throws ExceptionDate {// Initialise
                                                                     // l'objet
        init();

        if (annee < 1583) {
            throw new ExceptionDate("L'ann�e doit �tre sup�rieur � 1583");
        }
        if (mois <= 0 || mois > 12) {
            throw new ExceptionDate("Le Mois doit �tre compris entre 1 et 12");
        }
        if (jour < 0 || jour > dernierJourDuMois(mois, annee)) {
            throw new ExceptionDate("Le jour doit �tre compris entre 1 et " + dernierJourDuMois(mois, annee));
        }
        GregorianCalendar calendrier = new GregorianCalendar(annee, mois - 1, jour);
        setJour(jour);
        setMois(mois);
        setAnnee(annee);
        // System.out.println("tt " + calendrier.get(Calendar.DAY_OF_WEEK));
        // setJourSemaine(calendrier.get(Calendar.DAY_OF_WEEK));

        this.jourSemaine = calendrier.get(Calendar.DAY_OF_WEEK);
        this.semaine = calendrier.get(Calendar.WEEK_OF_YEAR);
        this.Heure = calendrier.get(Calendar.HOUR_OF_DAY);
        this.Minute = calendrier.get(Calendar.MINUTE);
    }

    /**
     * Constructeur de la date.
     */
    public Date() { // Initialise la date d'aujourd'hui
        init();

        GregorianCalendar aujourdhui = new GregorianCalendar();
        this.annee = aujourdhui.get(Calendar.YEAR);
        this.mois = aujourdhui.get(Calendar.MONTH) + 1;
        this.jour = aujourdhui.get(Calendar.DAY_OF_MONTH);
        this.jourSemaine = aujourdhui.get(Calendar.DAY_OF_WEEK);
        this.semaine = aujourdhui.get(Calendar.WEEK_OF_YEAR);
        this.Heure = aujourdhui.get(Calendar.HOUR_OF_DAY);
        this.Minute = aujourdhui.get(Calendar.MINUTE);
    }

    /**
     * Renvoie une chaine de caractere decrivant la date.
     */
    public String toString() { // Renvoie la date en String
        // return getJour() + "/" + getMois() + "/" + getAnnee();
        // System.out.println(getJourSemaine());
        return this.nomJourSemaine.get(getJourSemaine()) + " " + getJour() + " " + this.nomMois.get(getMois()) + " " + getAnnee() + " - " + this.Heure + ":" + this.Minute;
    }

    /**
     * Renvoie une chaine de caractere decrivant la date (jour mois annee)
     * Specialement faite pour le ProjetFrise
     * @return
     */
    public String toString2() {
        return this.getJour() + " " + this.nomMois.get(getMois()) + " " + getAnnee();
    }

    /**
     * Retourne la date tel que DD-MM-YYYY.
     * @return
     */
    public String ecrireDate() {
        return this.getJour() + "-" + this.getMois() + "-" + getAnnee();
    }

    /**
     * Renvoie true si la date est valide par rapport a une date normale
     * @return Un booleen
     */
    public boolean estValide() {
        return (getMois() > 0 && getMois() <= 12 && getJour() > 0 && getJour() <= dernierJourDuMois(getMois(), getAnnee()) && getAnnee() >= 1583);
    }

    /**
     * M�thode compareTo : retourne -1 si date courante inf�rieur au param�tre, 1 si l'inverse, 0 si �gales
     * Compare dans l'ordre :
     * annee
     * mois
     * jour
     */
    public int compareTo(Date date) {// Compare une date

        if (getAnnee() > date.getAnnee()) {
            return 1;
        }
        if (getAnnee() < date.getAnnee()) {
            return -1;
        }
        // Année correspondante
        if (getMois() > date.getMois()) {
            return 1;
        }
        if (getMois() < date.getMois()) {
            return -1;
        }
        // Mois correspondante
        if (getJour() > date.getJour()) {
            return 1;
        }
        if (getJour() < date.getJour()) {
            return -1;
        }
        // Jour Correspondante
        return 0;

    }
    

    /**
     * Renvoie vrai si l'annee est bissextile, faux sinon
     * @param annee
     * @return true si annee bissextile 
     */
    public static boolean estBissextile(int annee) {
        return (annee % 4 == 0 && annee % 100 != 0) || (annee % 400 == 0);
    }

    /**
     * Retourne le dernier jour du mois (entier)
     * @param mois
     * @param annee
     * @return
     */
    public static int dernierJourDuMois(int mois, int annee) {
        switch (mois) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                if (estBissextile(annee)) {
                    return 29;
                }
                else {
                    return 28;
                }
            default:
                return 0;
        }
    }

    /**
     * Permet de saisir une date, renvoie un objet Date
     * @param dateLu
     * @return
     * @throws ExceptionDate
     */
    public static Date lireDate(String dateLu) throws ExceptionDate {

        int jour, mois, annee;

        while (true) {

            StringTokenizer st = new StringTokenizer(dateLu, "-");

            //System.out.println(st.countTokens());

            if (st.countTokens() != 3) {
                System.out.println("[Erreur] La forme entr�e n'est pas �gal � la forme attendu (dd-mm-aaaa)");
                continue;
            }

            jour = Integer.parseInt(st.nextToken());
            mois = Integer.parseInt(st.nextToken());
            annee = Integer.parseInt(st.nextToken());

            if (annee < 1583) {
                System.out.println("L'ann�e doit �tre sup�rieur � 1583");
                continue;
            }
            if (mois <= 0 || mois > 12) {
                System.out.println("Le Mois doit �tre compris entre 1 et 12");
                continue;
            }
            if (jour < 0 || jour > dernierJourDuMois(mois, annee)) {
                System.out.println("Le jour doit �tre compris entre 1 et " + dernierJourDuMois(mois, annee));
                continue;
            }

            break;
        }

        return new Date(jour, mois, annee);

        /*
         * 
         * while(true){ System.out.println("Saisiser le jour :"); jour =
         * Clavier.lireInt(); if(jour > 0 && jour <= 31){ break; } }
         * while(true){ System.out.println("Saisiser le mois :"); mois =
         * Clavier.lireInt(); if(mois > 0 && mois <= 12){ break; } }
         * while(true){ System.out.println("Saisiser l'ann�e :"); annee =
         * Clavier.lireInt(); if(annee > 1583){ break; } } return new Date(jour,
         * mois, annee);
         */
    }

    /**
     * Retourne la date du lendemain de la date courante
     * @return
     * @throws ExceptionDate
     */
    public Date dateDuLendemain() throws ExceptionDate {

        int jourLendemain = this.jour + 1, moisLendemain = this.mois, anneeLendemain = this.annee;

        if (jourLendemain > dernierJourDuMois(this.mois, this.annee)) {
            moisLendemain = this.mois + 1;
            if (moisLendemain > 12) {
                moisLendemain = 1;
                anneeLendemain = this.annee + 1;
            }
            else {
                anneeLendemain = this.annee;
            }
            jourLendemain = 1;
        }
        else {
            moisLendemain = this.mois;
        }

        return new Date(jourLendemain, moisLendemain, anneeLendemain);

    }

    /**
     * Retourne la date d'hier de la date courante
     * @return
     * @throws ExceptionDate
     */
    public Date dateDeLaVeille() throws ExceptionDate {

        int jourLendemain = this.jour - 1, moisLendemain = this.mois, anneeLendemain = this.annee;

        if (jourLendemain < 1) {
            moisLendemain = this.mois - 1;
            if (moisLendemain < 1) {
                moisLendemain = 12;
                anneeLendemain = this.annee - 1;
            }
            else {
                anneeLendemain = this.annee;
            }
            jourLendemain = dernierJourDuMois(moisLendemain, anneeLendemain);
        }
        else {
            moisLendemain = this.mois;
        }

        return new Date(jourLendemain, moisLendemain, anneeLendemain);
    }

    /**
     * Retourne le premier jour de la semaine de la date courante
     * @return
     */
    public Date getPremierJourSemaine() {
        Date d = this;
        while (d.getJourSemaine() != 2) { //Lundi
            try {
                d = d.dateDeLaVeille();
            }
            catch (ExceptionDate e) {
                e.printStackTrace();
            }

        }

        return d;
    }

    /**
     * Retourne un booleen en fonction de si la date courante est celle d'aujourd'hui 
     * @return true si la date est la date d'ajourd'hui, false sinon
     */
    public boolean isToday() {
        Date today = new Date();
        if (this.jour == today.getJour() && this.mois == today.getMois() && this.annee == today.getAnnee()) {
            return true;
        }
        return false;
    }

    // Methode de getter et de Setter

    /**
     * 
     * @return
     */
    public int getHeure() {
        return Heure;
    }

    /**
     * 
     * @param heure
     */
    public void setHeure(int heure) {
        Heure = heure;
    }

    /**
     * 
     * @return
     */
    public int getMinute() {
        return Minute;
    }

    /**
     * 
     * @param minute
     */
    public void setMinute(int minute) {
        Minute = minute;
    }

    /**
     * 
     * @return
     */
    public int getJour() {
        return jour;
    }

    /**
     * 
     * @param jour
     */
    public void setJour(int jour) {
        this.jour = jour;
    }

    /**
     * 
     * @return
     */
    public int getMois() {
        return mois;
    }

    /**
     * 
     * @param mois
     */
    public void setMois(int mois) {
        this.mois = mois;
    }

    /**
     * 
     * @return
     */
    public int getAnnee() {
        return annee;
    }

    /**
     * 
     * @param annee
     */
    public void setAnnee(int annee) {
        this.annee = annee;
    }

    /**
     * 
     * @return
     */
    public int getJourSemaine() {
        return jourSemaine;
    }

    /**
     * 
     * @param jourSemaine
     */
    public void setJourSemaine(int jourSemaine) {
        this.jourSemaine = jourSemaine;
    }

    /**
     * 
     * @return
     */
    public int getSemaine() {
        return semaine;
    }

    /**
     * 
     * @param semaine
     */
    public void setSemaine(int semaine) {
        this.semaine = semaine;
    }

}
