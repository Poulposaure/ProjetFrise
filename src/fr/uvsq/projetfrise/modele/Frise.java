package fr.uvsq.projetfrise.modele;

import java.io.Serializable;
import java.util.Collection;
import java.util.TreeSet;

import fr.uvsq.projetfrise.outils.ExceptionFrise;

/**
 * Frise chronologique
 * 
 * Une frise chronologique est d�finie par son titre, sa date de d�but, sa date de fin, sa p�riode et sa liste d'�v�nements.
 * La liste d'�v�nements est rang�e dans un TreeSet, et sont donc tri�s, afin que l'affichage se fasse en fonction de la date.
 * 
 * La date de fin et date de d�but d'une frise sont des ann�es.
 * 
 * La p�riode d'une frise est l'intervalle pour lequel une ann�e s'affichera dans l'en-tete de la frise
 * 
 * @author fcledic & paul
 *
 */
public class Frise implements Serializable {

    private static final long serialVersionUID = 1L;
    private String titre;
    private int dateDebut;
    private int dateFin;
    private int periode;
    private TreeSet<Evenement> listeEvents;

    /**
     * Constructeur de la Frise. Prend en compte tous les champs.
     * @param titre
     * @param dateDebut
     * @param dateFin
     * @param periode
     * @param listeEvents Les �v�nements sont tri�s
     * @throws ExceptionFrise La p�riode inf�rieure � 0
     */
    public Frise(String titre, int dateDebut, int dateFin, int periode, TreeSet<Evenement> listeEvents) throws ExceptionFrise {
        if (periode<0)
            throw new ExceptionFrise("La periode doit etre superieure a 0.");
        this.setTitre(titre);
        this.setDateDebut(dateDebut);
        this.setDateFin(dateFin);
        this.periode = periode;
        this.setListeEvents(listeEvents);
    }

    /**
     * Constructeur de la Frise. Ne prend pas en compte la liste d'evenements de la frise.
     * @param titre
     * @param dateDebut
     * @param dateFin
     * @param periode
     * @throws ExceptionFrise La p�riode inf�rieure � 0
     */
    public Frise(String titre, int dateDebut, int dateFin, int periode) throws ExceptionFrise {
        if (periode<0)
            throw new ExceptionFrise("La periode doit etre superieure a 0.");
        this.setTitre(titre);
        this.setDateDebut(dateDebut);
        this.setDateFin(dateFin);
        this.periode = periode;
        this.listeEvents = new TreeSet<Evenement>();
    }

    /**
     * Retourne une chaine de caracteres decrivant la frise chronologique
     */
    public String toString() {
        String retour = "Titre : " + titre + "\nDate : de " + dateDebut + " a " + dateFin + "\n";
        if (listeEvents.isEmpty())
            return retour;
        retour += "Liste des evenements (" + this.listeEvents.size() + ") : \n";
        for (Evenement e : this.listeEvents)
            retour += e.toString();
        retour += "\n";
        return retour;
    }

    /**
     * Ajoute un evenement a la frise
     * @param evt Evenement que l'on souhaite ajouter
     */
    public void ajout(Evenement evt) {
        this.listeEvents.add(evt);
    }

    /**
     * Accesseur : retourne les evenements de la frise
     * @return Une colection d'Evenement
     */
    public TreeSet<Evenement> getListeEvents() {
        return listeEvents;
    }

    /**
     * Modifieur : modifie la liste d'evenements de la frise. Les anciens evenements sont ecrases par les nouveaux
     * @param listeEvents liste d'evenement que l'on souhaite ajouter a la frise
     */
    public void setListeEvents(TreeSet<Evenement> listeEvents) {
        this.listeEvents = listeEvents;
    }

    /**
     * Accesseur : retourne le titre de la frise.
     * @return Une chaine de caractere, le titre de la frise
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Modifieur : modifie le titre de la frise
     * @param titre
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * Accesseur : retourne la date de fin de la frise.
     * @return La date de fin de la frise
     */
    public int getDateFin() {
        return dateFin;
    }

    /**
     * Modifieur : modifie la date de fin de la frise.
     * @param dateFin
     */
    public void setDateFin(int dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Accesseur : retourne la date de debut de la frise.
     * @return La date de debut de la frise
     */
    public int getDateDebut() {
        return dateDebut;
    }

    /**
     * Modifieur : modifie la date de debut de la frise
     * @param dateDebut
     */
    public void setDateDebut(int dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Accesseur : retourne la periode de la frise.
     * Intervalle pour lequel les ann�es seront affich�es dans l'en-tete de la frise chronologique
     * @return La periode
     */
    public int getPeriode() {
        return periode;
    }

    /**
     * Modifieur : modifie la periode de la frise
     * @param periode
     */
    public void setPeriode(int periode) {
        this.periode = periode;
    }

}
