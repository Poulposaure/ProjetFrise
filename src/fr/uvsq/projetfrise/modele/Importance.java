package fr.uvsq.projetfrise.modele;

/**
 * Importance
 * 
 * Evalue le poids d'un evenement. Plus l'evenement est important, plus le chiffre correspondant est faible.
 * Important : 0
 * Fort : 1
 * Moyen : 2
 * Faible : 3
 * 
 * 
 * @author fcledic & paul
 *
 */
public enum Importance {

    IMPORTANT, FORT, MOYEN, FAIBLE ;
	
}
